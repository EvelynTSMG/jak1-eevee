("SNK.DGO"
  ("static-screen.o" "static-screen")
  ("world-snake.go" "world-snake")

  ;; Dependencies
  ;; General
  ("snake-part.o" "snake-part") ;; Particles

  ;; Warpgate
  ("villagep-obs.o" "villagep-obs")
  ("village1-part2.o" "village1-part2")
  ("warp-gate-switch-ag.go" "warp-gate-switch")
  ("warpgate-ag.go" "warpgate")

  ;; Textures???
  ("tpage-358.go" "tpage-358")
  ("tpage-659.go" "tpage-659")
  ("tpage-629.go" "tpage-629")
  ("tpage-630.go" "tpage-630")
  ("tpage-710.go" "tpage-710")
  ("tpage-842.go" "tpage-842")
  ("tpage-711.go" "tpage-711")
  ("tpage-712.go" "tpage-712")

  ;; Flutflut
  ("blocking-plane.o" "blocking-plane") ;; Restrict the bird
  ("flut-part.o" "flut-part")
  ("flutflut.o" "flutflut")
  ("target-flut.o" "target-flut")
  ("flut-saddle-ag.go" "flut-saddle")
  ("eichar-flut+0-ag.go" "eichar-flut+0")

  ;; Tube sliding
  ("target-tube.o" "target-tube")
  ("eichar-tube+0-ag.go" "eichar-tube+0")
  )